package de.fearnixx.jeak.channelmgmt.ondemand;

import de.fearnixx.jeak.channelmgmt.ondemand.conf.DemandSet;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.service.teamspeak.IUserService;

public class NewDemandChannelCommand {

    private ChannelDemandService demandService;

    public static ICommandSpec getCommandSpec() {
        return Commands.commandSpec("demand", "channelmgmt:demand")
                .parameters(Commands.paramSpec("setName", String.class))
                .permission("channelmgmgt.demand.demand")
                .executor(new NewDemandChannelCommand()::execute)
                .build();
    }

    private void execute(ICommandExecutionContext context) throws CommandParameterException {
        String setName = context.getRequiredOne("setName", String.class);
        DemandSet demandSet = demandService.getSetByName(setName).orElse(null);

        if (demandSet == null) {
            String errorMessage = demandService.localeUnit.getContext(context.getSender()).getMessage("demand.unknown_demand_set");
            throw new CommandParameterException(errorMessage, "set-name", setName);
        }

        demandService.demandChannel(demandSet, context.getSender());
    }

    public void setService(ChannelDemandService demandService) {
        this.demandService = demandService;
    }
}


