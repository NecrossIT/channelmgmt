package de.fearnixx.jeak.channelmgmt.ondemand.conf;

import de.fearnixx.jeak.channelmgmt.util.PropUtil;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.QueryCommands;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.teamspeak.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class DemandPaginator {

    private static final Logger logger = LoggerFactory.getLogger(DemandPaginator.class);

    @Inject
    private IDataCache cache;

    @Inject
    private IServer server;

    private DemandSetPersistence store;

    public DemandPaginator(DemandSetPersistence store) {
        this.store = store;
    }

    public void paginatedCreate(DemandSet set, Consumer<Integer> parentIdConsumer) {
        int groupAtCount = set.getGroupAtCount();
        if (groupAtCount == 0) {
            parentIdConsumer.accept(set.getSubjectParent());

        } else {
            final List<Integer> channelsForSet = store.getChannelsForSet(set.getName());
            if (channelsForSet.size() % groupAtCount == 0) {
                // There is no space for another private channel => Create a new block

                IDataHolder block = new BasicDataHolder();
                block.setProperty(PropertyKeys.Channel.PARENT, set.getSubjectParent());
                block.setProperty(PropertyKeys.Channel.NAME, UUID.randomUUID().toString().substring(0, 9));
                PropUtil.fixPropsForCreation(block);

                QueryBuilder createRequest = IQueryRequest.builder().command(QueryCommands.CHANNEL.CHANNEL_CREATE);
                block.getValues().forEach(createRequest::addKey);
                createRequest.onError(err -> {
                    logger.warn("Could not create block channel for pagination! {} - {}", err.getErrorCode(), err.getErrorMessage());
                });
                createRequest.onSuccess(answer -> {
                    Optional<String> optCID = answer.getDataChain().get(0).getProperty("cid");
                    if (!optCID.isPresent()) {
                        logger.error("TS3 did not return the created channel id!");
                    } else {
                        parentIdConsumer.accept(Integer.parseInt(optCID.get()));
                    }
                });
                server.getConnection().sendRequest(createRequest.build());

            } else {
                // We need to paginate
                final Map<Integer, IChannel> channelMap = cache.getChannelMap();
                final IChannel blockWithFreeSpace = channelsForSet.stream()
                        .map(channelMap::get)
                        .filter(Objects::nonNull)
                        .filter(block -> block.getSubChannels().size() < groupAtCount)
                        .findFirst()
                        .orElseThrow(() -> new IllegalStateException("No channel with fewer than max sub channels found?!"));

                parentIdConsumer.accept(blockWithFreeSpace.getID());
            }
        }
    }

    public void deleteOrphanedBlocks(DemandSet set) {
        if (set.getGroupAtCount() > 0) {
            IChannel channel = cache.getChannelMap().getOrDefault(set.getSubjectParent(), null);
            if (channel != null) {
                List<IChannel> forDeletion = channel.getSubChannels()
                        .stream()
                        .filter(block -> block.getSubChannels().isEmpty())
                        .collect(Collectors.toList());
                logger.debug("Will be deleting: {}", forDeletion);

                QueryBuilder deleteBuilder = IQueryRequest.builder().command(QueryCommands.CHANNEL.CHANNEL_DELETE);
                forDeletion.forEach(block -> {
                    deleteBuilder.addKey(PropertyKeys.Channel.ID, block.getID());
                    deleteBuilder.commitChainElement();
                });
                server.getConnection().sendRequest(deleteBuilder.build());

            } else {
                logger.info("Cannot check for orphaned private channel blocks: Parent not cached.");
            }
        } else {
            logger.debug("Not checking channels for set {} for deletions: Not grouping.", set.getName());
        }
    }
}
