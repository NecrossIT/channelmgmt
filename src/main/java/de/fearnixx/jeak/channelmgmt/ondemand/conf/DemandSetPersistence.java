package de.fearnixx.jeak.channelmgmt.ondemand.conf;

import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.Config;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class DemandSetPersistence extends Configurable {

    @Inject
    @Config(id = "set-data", category = "data")
    private IConfig configRef;
    private boolean dirty = false;

    public DemandSetPersistence() {
        super(DemandSetPersistence.class);
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
        }
    }

    public synchronized void saveIfModified() {
        if (dirty) {
            saveConfig();
        }
    }

    public synchronized void saveChannelCreatedFor(String setName, UUID profileUUID, int channelId) {
        getConfig().getNode(setName, profileUUID.toString()).setInteger(channelId);
        markDirty();
    }

    public synchronized Optional<Integer> getChannelFor(String setName, UUID profileUUID) {
        return getConfig().getNode(setName, profileUUID.toString()).optInteger();
    }

    public synchronized List<Integer> getChannelsForSet(String setName) {
        return getConfig().getNode(setName)
                .optMap()
                .orElseGet(Collections::emptyMap)
                .values()
                .stream()
                .map(IValueHolder::optInteger)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    protected void markDirty() {
        dirty = true;
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return null;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        root.getNode("@placeholder").setString("_");
        return true;
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }
}
