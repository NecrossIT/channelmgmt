package de.fearnixx.jeak.channelmgmt.ondemand;

import de.fearnixx.jeak.channelmgmt.ondemand.conf.DemandPaginator;
import de.fearnixx.jeak.channelmgmt.ondemand.conf.DemandSet;
import de.fearnixx.jeak.channelmgmt.ondemand.conf.DemandSetPersistence;
import de.fearnixx.jeak.channelmgmt.util.PropUtil;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.profile.IProfileService;
import de.fearnixx.jeak.profile.IUserProfile;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.service.notification.INotification;
import de.fearnixx.jeak.service.notification.INotificationService;
import de.fearnixx.jeak.service.notification.Lifespan;
import de.fearnixx.jeak.service.notification.Urgency;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.PropertyKeys.Channel;
import de.fearnixx.jeak.teamspeak.QueryCommands;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.teamspeak.query.QueryBuilder;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ChannelDemandService extends Configurable {

    private static final String DEFAULT_CONFIG_URI = "/channelmgmt/channel_demand.json";
    private static final Logger logger = LoggerFactory.getLogger(ChannelDemandService.class);
    private static final String PROFILE_OWNERSHIP_OPTION = "channelmgmgt:channel_owner:";

    @Inject
    private IServer server;

    @Inject
    @Config(id = "channel_demand")
    private IConfig configRef;

    @Inject
    @LocaleUnit(value = "channelmgmt", defaultResource = "channelmgmt/lang.json")
    public ILocalizationUnit localeUnit;

    @Inject
    private IEventService eventService;

    @Inject
    private IInjectionService injectionService;

    @Inject
    private IProfileService profileService;

    @Inject
    private IDataCache cache;

    @Inject
    private INotificationService notificationService;

    private final DemandSetPersistence store = new DemandSetPersistence();
    private final DemandPaginator paginator = new DemandPaginator(store);

    private final JoinWatcher joinWatcher = new JoinWatcher(this);
    private final Map<String, DemandSet> demandSets = new HashMap<>();

    public ChannelDemandService() {
        super(ChannelDemandService.class);
    }

    @Listener
    public void onPreInit(IBotStateEvent.IPreInitializeEvent event) {
        injectionService.injectInto(store);
        eventService.registerListener(store);
        injectionService.injectInto(paginator);
        eventService.registerListener(paginator);
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
            return;
        }
        getConfig().getNode("sets")
                .optMap()
                .orElseGet(Collections::emptyMap)
                .entrySet()
                .stream()
                .map(e -> {
                    DemandSet set = new DemandSet(e.getKey());
                    if (!set.load(e.getValue())) {
                        logger.warn("Failed to load set: {}", e.getKey());
                        return null;
                    } else {
                        logger.debug("Loaded set: {}", e.getKey());
                        return set;
                    }
                })
                .filter(Objects::nonNull)
                .forEach(set -> demandSets.put(set.getName(), set));

        eventService.registerListener(joinWatcher);
    }

    public Optional<DemandSet> getSetByName(String demandSetName) {
        return demandSets.values()
                .stream()
                .filter(set -> set.getName().toLowerCase().equals(demandSetName))
                .findFirst();
    }

    public Optional<DemandSet> getSetForWatchedChannel(int channelId) {
        return demandSets.values()
                .stream()
                .filter(set -> set.getJoinWatchChannel() == channelId)
                .findFirst();
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONFIG_URI;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        return false;
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }

    public synchronized void demandChannel(final DemandSet set, final IClient channelOwner) {
        if (!isAllowed(set, channelOwner)) {
            logger.info("Client {} is not allowed to demand channels from set {}.", channelOwner, set.getName());
            sendNotAllowedNotification(set, channelOwner);
            return;
        }

        int alreadyOwnsId = getOwnedChannel(set, channelOwner);
        if (alreadyOwnsId > 0) {
            if (channelOwner.getChannelID() != alreadyOwnsId) {
                logger.debug("Moving client to his already owned channel: {} -> {} for set {}", channelOwner, alreadyOwnsId, set.getName());
                final IQueryRequest moveCommand = channelOwner.moveToChannel(alreadyOwnsId);
                moveCommand.onError(err -> {
                    if (err.getErrorCode() == 768) {
                        logger.warn("Cache appears to be outdated. Creating demand-channel for {}", channelOwner);
                        paginator.paginatedCreate(set, parent -> {
                            createChannelFor(set, channelOwner, parent);
                        });
                    }
                });
                moveCommand.onSuccess(answer -> sendMovedNotification(channelOwner));
                server.getConnection().sendRequest(moveCommand);
            } else {
                logger.warn("Not moving client \"{}\" - already in the owned channel.", channelOwner);
            }
        } else {
            paginator.paginatedCreate(set, parent -> {
                createChannelFor(set, channelOwner, parent);
            });
        }
    }

    private boolean isAllowed(DemandSet set, IClient channelOwner) {
        final List<Integer> serverGroups = channelOwner.getGroupIDs();
        final boolean allowedByGroup = set.getAllowedServerGroups()
                .stream()
                .anyMatch(serverGroups::contains);
        return allowedByGroup || channelOwner.hasPermission("channelmgmgt.demand." + set.getName());
    }

    private int getOwnedChannel(DemandSet set, IClient channelOwner) {
        int alreadyOwnsId = -1;

        final Optional<IUserProfile> optProfile = profileService.getProfile(channelOwner.getClientUniqueID());
        if (optProfile.isPresent()) {
            final IUserProfile profile = optProfile.get();
            final Integer prevOwnershipId = store.getChannelFor(set.getName(), profile.getUniqueId()).orElse(-1);
            logger.debug("Profile exists for client {} and store returned: {}", channelOwner, prevOwnershipId);

            if (prevOwnershipId >= 0) {
                final IChannel channel = cache.getChannelMap().getOrDefault(prevOwnershipId, null);
                if (channel != null) {
                    logger.debug("Owned channel exists.");
                    alreadyOwnsId = channel.getID();
                } else {
                    logger.info("Stored channel: {} no longer exists.", prevOwnershipId);
                }
            }
        }
        return alreadyOwnsId;
    }

    private void createChannelFor(DemandSet set, IClient channelOwner, int channelParent) {
        logger.debug("Creating private channel for: {}", channelOwner);
        final String goBackToChannel = server.getConnection().getWhoAmI().getProperty("client_channel_id").orElse("0");
        final String botClientID = server.getConnection().getWhoAmI().getProperty("client_id").orElse("-1");

        IDataHolder channel = new BasicDataHolder();
        set.getSubjectProperties().forEach(channel::setProperty);
        Map<String, Integer> permissions = set.getSubjectPermissions();
        setCreationProperties(set, channel, channelParent);

        Optional<String> optIconId = channel.getProperty(Channel.ICON_ID);
        if (optIconId.isPresent()) {
            try {
                permissions.put("i_icon_id", Integer.parseInt(optIconId.get()));
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid icon id: " + optIconId.get());
            }
        }

        PropUtil.fixPropsForCreation(channel);

        QueryBuilder createBuilder = IQueryRequest.builder()
                .command(QueryCommands.CHANNEL.CHANNEL_CREATE);
        channel.getValues().forEach(createBuilder::addKey);
        boolean requirePerms = !permissions.isEmpty();

        createBuilder.onError(err -> {
            logger.warn("Failed to create on-demand channel: {} - {}", err.getErrorCode(), err.getErrorMessage());
        }).onSuccess(data -> {
            Optional<String> optChannelID = data.getDataChain().get(0).getProperty(Channel.ID);
            if (!optChannelID.isPresent()) {
                logger.warn("TS3 did not return the created channel ID! Cannot continue...");
            } else {
                String createdChannelIdStr = optChannelID.get();
                int createdChannelId = Integer.parseInt(createdChannelIdStr);
                logger.debug("Moving client {} to channel {} and assigning channel group: {}", channelOwner, createdChannelIdStr, set.getSubjectChannelGroup());
                server.getConnection().sendRequest(channelOwner.moveToChannel(createdChannelId));
                server.getConnection().sendRequest(channelOwner.setChannelGroup(createdChannelId, set.getSubjectChannelGroup()));
                sendCreatedNotification(channel, channelOwner);
                saveAssignedChannel(channelOwner, set, createdChannelId);

                if (requirePerms) {
                    QueryBuilder permSetBuilder = IQueryRequest.builder()
                            .command(QueryCommands.PERMISSION.CHANNEL_PERMISSION_ADD);

                    permSetBuilder.addKey(Channel.ID, createdChannelIdStr);
                    permissions.forEach((permSID, value) -> {
                        permSetBuilder.addKey("permsid", permSID);
                        permSetBuilder.addKey(PropertyKeys.Permission.VALUE_SHORT, Math.abs(value));
                        permSetBuilder.addKey(PropertyKeys.Permission.FLAG_SKIP, "0");
                        permSetBuilder.addKey(PropertyKeys.Permission.FLAG_NEGATED, value < 0 ? "1" : "0");
                        permSetBuilder.commitChainElement();
                    });

                    permSetBuilder.onError(err -> {
                        logger.warn("Failed to apply permissions to demanded channel: {} - {}", err.getErrorCode(), err.getErrorMessage());
                    });
                    permSetBuilder.onSuccess(answer -> {
                        logger.debug("Successfully applied permissions for demanded channel.");
                    });

                    server.getConnection().sendRequest(permSetBuilder.build());
                }
            }

            if (set.getSubjectType() == IChannel.ChannelPersistence.TEMPORARY) {
                // When creating temporary channels, the server will move our client.
                // This is even true for query clients!
                final IQueryRequest goBackReq = IQueryRequest.builder()
                        .command(QueryCommands.CLIENT.CLIENT_MOVE)
                        .addKey(PropertyKeys.Client.ID, botClientID)
                        .addKey(Channel.ID, goBackToChannel)
                        .build();
                server.getConnection().sendRequest(goBackReq);
            }
        });

        server.getConnection().sendRequest(createBuilder.build());
    }

    private void setCreationProperties(DemandSet set, IDataHolder channel, int parentId) {
        channel.setProperty(Channel.NAME, "PrivateChannel [" + UUID.randomUUID().toString().substring(0, 8) + "]");
        channel.setProperty(Channel.PARENT, Integer.toString(parentId));

        if (set.getSubjectType() == IChannel.ChannelPersistence.PERMANENT) {
            channel.setProperty(Channel.FLAG_PERMANENT, "1");
            channel.setProperty(Channel.FLAG_SEMI_PERMANENT, "0");
        } else if (set.getSubjectType() == IChannel.ChannelPersistence.SEMI_PERMANENT) {
            channel.setProperty(Channel.FLAG_PERMANENT, "0");
            channel.setProperty(Channel.FLAG_SEMI_PERMANENT, "1");
        } else {
            channel.setProperty(Channel.FLAG_PERMANENT, "0");
            channel.setProperty(Channel.FLAG_SEMI_PERMANENT, "0");
            channel.setProperty(Channel.DELETE_DELAY, set.getSubjectTTL());
        }
    }

    private void saveAssignedChannel(IClient channelOwner, DemandSet set, int createdChannelId) {
        final Optional<IUserProfile> optProfile = profileService.getOrCreateProfile(channelOwner.getClientUniqueID());
        if (optProfile.isPresent()) {
            final IUserProfile profile = optProfile.get();
            profile.setOption(PROFILE_OWNERSHIP_OPTION + set.getName(), Integer.toString(createdChannelId));
            store.saveChannelCreatedFor(set.getName(), profile.getUniqueId(), createdChannelId);

            logger.debug("Saved \"{}\" as owner of channel \"{}\" for set \"{}\"", channelOwner, createdChannelId, set.getName());
        } else {
            logger.warn("Failed to store channel ownership information in profile for client {} and set {}", channelOwner, set.getName());
        }
    }

    private void sendMovedNotification(IClient channelOwner) {
        final String message = localeUnit.getContext(channelOwner.getCountryCode())
                .optMessage("demand.private_moved_to", channelOwner.getValues())
                .orElseThrow(() -> new IllegalStateException("Missing message: demand.private_moved_to"));
        notificationService.dispatch(
                INotification.builder()
                        .shortText(message)
                        .addRecipient(channelOwner)
                        .lifespan(Lifespan.SHORTER)
                        .urgency(Urgency.BASIC)
                        .build());
    }

    private void sendNotAllowedNotification(DemandSet set, IClient channelOwner) {
        Map<String, String> params = new HashMap<>();
        channelOwner.getValues().forEach(params::put);
        params.put("set_name", set.getName());
        final String message = localeUnit.getContext(channelOwner.getCountryCode())
                .optMessage("demand.not_allowed", params)
                .orElseThrow(() -> new IllegalStateException("Missing message: demand.not_allowed!"));
        notificationService.dispatch(
                INotification.builder()
                        .shortText(message)
                        .addRecipient(channelOwner)
                        .lifespan(Lifespan.SHORTER)
                        .urgency(Urgency.BASIC)
                        .build());
    }

    private void sendCreatedNotification(IDataHolder createdChannel, IClient channelOwner) {
        final boolean hasPassword = createdChannel.hasProperty(Channel.PASSWORD);
        final String msgId = hasPassword ? "demand.private_created.pw" : "demand.private_created";

        final Map<String, String> msgParams = new HashMap<>();
        channelOwner.getValues().forEach(msgParams::put);
        createdChannel.getValues().forEach(msgParams::put);
        final String message = localeUnit.getContext(channelOwner.getCountryCode())
                .optMessage(msgId, msgParams)
                .orElseThrow(() -> new IllegalStateException("Missing message: demand.private_channel!"));
        notificationService.dispatch(
                INotification.builder()
                        .shortText(message)
                        .addRecipient(channelOwner)
                        .lifespan(Lifespan.SHORTER)
                        .urgency(Urgency.BASIC)
                        .build());
    }
}
