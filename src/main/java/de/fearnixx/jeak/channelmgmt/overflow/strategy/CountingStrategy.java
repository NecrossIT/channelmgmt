package de.fearnixx.jeak.channelmgmt.overflow.strategy;

import de.fearnixx.jeak.channelmgmt.overflow.OverflowAction;
import de.fearnixx.jeak.channelmgmt.overflow.state.CountingState;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.BasicDataHolder;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountingStrategy extends OverflowStrategy<CountingState> {

    private static final Pattern HASGROUP_PATTERN = Pattern.compile("\\(\\\\d\\+\\)");
    private static final Pattern HASREPLACEMENT_PATTERN = Pattern.compile("%i%");

    private static final Logger logger = LoggerFactory.getLogger(CountingStrategy.class);

    private Pattern numberPattern;
    private String namingSchema;
    private Integer maxChildren;

    @Override
    public boolean fromNode(IConfigNode node) {
        String numberPatternSource = node.getNode("childPattern").optString(".*(\\d+).*");

        if (!HASGROUP_PATTERN.matcher(numberPatternSource).find()) {
            logger.error("Invalid number detection pattern (must contain exactly one group of \"(\\\\d+)\": {}", numberPatternSource);
            return false;
        } else {
            numberPattern = Pattern.compile(numberPatternSource);
        }

        namingSchema = node.getNode("namingSchema").optString("Auto %i%");
        if (!HASREPLACEMENT_PATTERN.matcher(namingSchema).find()) {
            logger.error("Naming schema has no replacement placeholder! (\"%i%\"): {}", namingSchema);
            return false;
        }

        maxChildren = node.getNode("maxChildren").optInteger(10);
        return super.fromNode(node);
    }

    @Override
    protected CountingState createState(IChannel parent) {
        return new CountingState(parent);
    }

    @Override
    protected boolean matches(IChannel child) {
        return numberPattern.matcher(child.getName()).find();
    }

    @Override
    protected void countChildren(IChannel child, CountingState state) {
        Matcher matcher = numberPattern.matcher(child.getName());

        if (matcher.find()) {
            String numberStr = matcher.group(1);
            Integer number = Integer.parseInt(numberStr);
            state.assignNumber(number, child.getID());
            super.countChildren(child, state);
        } else {
            throw new IllegalStateException("No match found for channel although it should be counted! This is not good!");
        }
    }

    @Override
    protected Optional<IDataHolder> createChannel(CountingState state) {
        Optional<Integer> optNumber = nextNumber(state);

        if (optNumber.isPresent()) {
            Integer number = optNumber.get();
            IDataHolder channelProps = new BasicDataHolder().copyFrom(getChannelProperties());
            channelProps.setProperty(PropertyKeys.Channel.PARENT, state.getParent().getID());

            String channelName = namingSchema.replaceAll("%i%", number.toString());
            channelProps.setProperty(PropertyKeys.Channel.NAME, channelName);

            // Set sorting ID
            Integer sortBy;
            if (number == 1) {
                sortBy = 0;
            } else {
                sortBy = state.getChannelIdForNumber(number - 1);
            }
            channelProps.setProperty(PropertyKeys.Channel.ORDER, sortBy);
            return Optional.of(channelProps);

        } else {
            logger.warn("Cannot create any more channels for parent {}/{}", state.getParent().getName(), state.getParent().getID());
            return Optional.empty();
        }
    }

    private Optional<Integer> nextNumber(CountingState state) {
        Set<Integer> usedNumbers = state.getUsedNumbers();

        if (usedNumbers.size() < maxChildren) {
            for (int i = 1; i <= maxChildren; i++) {
                if (!usedNumbers.contains(i)) {
                    return Optional.of(i);
                }
            }
        }

        return Optional.empty();
    }

    @Override
    protected void deleteChannel(Integer channelId, OverflowAction action, CountingState state) {
        state.untrack(channelId);
        super.deleteChannel(channelId, action, state);
    }
}
