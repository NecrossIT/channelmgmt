package de.fearnixx.jeak.channelmgmt.overflow;

import de.fearnixx.jeak.teamspeak.data.IDataHolder;

import java.util.List;
import java.util.Map;

public class OverflowCreateInfo {

    private final IDataHolder properties;
    private final List<String> enforcements;
    private final Map<String, Integer> permissions;

    public OverflowCreateInfo(IDataHolder properties, List<String> enforcements, Map<String, Integer> permissions) {
        this.properties = properties;
        this.enforcements = enforcements;
        this.permissions = permissions;
    }

    public IDataHolder getProperties() {
        return properties;
    }

    public List<String> getEnforcements() {
        return enforcements;
    }

    public Map<String, Integer> getPermissions() {
        return permissions;
    }
}
