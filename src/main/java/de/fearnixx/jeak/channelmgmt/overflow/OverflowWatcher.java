package de.fearnixx.jeak.channelmgmt.overflow;

import de.fearnixx.jeak.IBot;
import de.fearnixx.jeak.channelmgmt.enforcement.IEnforcementService;
import de.fearnixx.jeak.channelmgmt.util.PropUtil;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.IRawQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.permission.base.IPermissionService;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.QueryCommands;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.teamspeak.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class OverflowWatcher {

    private static final Logger logger = LoggerFactory.getLogger(OverflowWatcher.class);

    @Inject
    private IDataCache dataCache;

    @Inject
    private IBot bot;

    @Inject
    private IEnforcementService enforcementService;

    @Inject
    private IPermissionService permSvc;

    private OverflowManager manager;

    private List<Integer> watchedChannels = new ArrayList<>();

    public OverflowWatcher(OverflowManager manager) {
        this.manager = manager;
    }

    @Listener
    public void onChannelsUpdated(IQueryEvent.IDataEvent.IRefreshChannels event) {
        dataCache.getChannels()
                .stream()
                .filter(channel -> watchedChannels.contains(channel.getID()))
                .forEach(this::check);
    }

    private void check(IChannel parent) {
        logger.debug("Checking overflow stats for {}/{}", parent.getName(), parent.getID());
        OverflowAction action = new OverflowAction();
        manager.runForChannel(parent.getID(), set -> set.eval(parent, action));
        processAction(action);
    }

    private void processAction(OverflowAction action) {

        if (action.getForDeletion().isEmpty()) {
            logger.debug("No deletion actions.");
        } else {
            logger.info("Running {} deletion actions", action.getForDeletion().size());
            action.getForDeletion()
                    .forEach(this::runDelete);
        }

        if (action.getForCreation().isEmpty()) {
            logger.debug("No creation actions.");
        } else {
            logger.info("Running {} creation action", action.getForCreation().size());
            action.getForCreation()
                    .forEach(this::runCreate);
        }
    }

    private void runDelete(Integer channelId) {
        bot.getServer().getConnection().sendRequest(
                IQueryRequest.builder()
                        .command("channeldelete")
                        .addKey(PropertyKeys.Channel.ID, channelId)
                        .addKey("force", "1")
                        .build()
        );
    }

    private void runCreate(OverflowCreateInfo info) {
        final Map<String, Integer> channelPerms = new HashMap<>(info.getPermissions());
        final IDataHolder channelProps = info.getProperties();
        PropUtil.fixPropsForCreation(channelProps);

        // Since server version 3.9.0, setting the channel icon property is no longer supported.
        Optional<String> optIconId = channelProps.getProperty(PropertyKeys.Channel.ICON_ID);
        if (optIconId.isPresent()) {
            channelProps.setProperty(PropertyKeys.Channel.ICON_ID, null);
            try {
                channelPerms.put("i_icon_id", Integer.parseInt(optIconId.get()));
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Cannot create channel with invalid icon ID: " + optIconId.get());
            }
        }

        QueryBuilder createRequest = buildChannelCreateCommand(channelProps);
        final boolean permSetCommandRequired = !channelPerms.isEmpty();

        createRequest.onSuccess(answer -> {
            logger.debug("Successfully created overflow-subchannel.");
            // Get channel ID
            List<IDataHolder> chain = answer.getDataChain();
            Optional<String> optChannelId = !chain.isEmpty() ?
                    chain.get(0).getProperty(PropertyKeys.Channel.ID)
                    : Optional.empty();

            if (!optChannelId.isPresent()) {
                logger.error("Could not get cid from channelcreate answer!");
                return;
            }
            final int createdChannelId = Integer.parseInt(optChannelId.get());

            // Add to enforcements
            info.getEnforcements().forEach(name -> {
                logger.debug("Adding generated channel ({}) to enforcement: {}", createdChannelId, name);
                enforcementService.enableSetOn(createdChannelId, name);
            });

            // Run delayed actions (permissions & enforcement)
            if (permSetCommandRequired) {
                QueryBuilder permSetRequest = buildChannelPermCommand(channelPerms, createdChannelId);
                answer.getConnection().sendRequest(permSetRequest.build());
            } else {
                enforcementService.runForChannel(createdChannelId);
            }
        });
        createRequest.onError(answer -> {
            IRawQueryEvent.IMessage.IErrorMessage error = answer.getError();
            logger.warn("Failed to create overflow-subchannel: {} {}", error.getCode(), error.getMessage());
        });

        bot.getServer().getConnection().sendRequest(createRequest.build());
    }

    private QueryBuilder buildChannelPermCommand(Map<String, Integer> channelPerms, int channelId) {
        QueryBuilder request = IQueryRequest.builder();
        request.command(QueryCommands.PERMISSION.CHANNEL_PERMISSION_ADD);

        request.addKey(PropertyKeys.Channel.ID, channelId);
        channelPerms.forEach((permSID, value) -> {
            request.addKey("permsid", permSID);
            request.addKey(PropertyKeys.Permission.VALUE_UNABBREVIATED, Math.abs(value));
            request.addKey(PropertyKeys.Permission.FLAG_NEGATED, value < 0 ? "1" : "0");
            request.addKey(PropertyKeys.Permission.FLAG_SKIP, "0");
            request.commitChainElement();
        });

        request.onSuccess(answer -> {
            logger.debug("Successfully set permissions on overflow-subchannel.");
        });
        request.onError(answer -> {
            IRawQueryEvent.IMessage.IErrorMessage error = answer.getError();
            logger.warn("Failed to set perms on overflow-subchannel: {} {}", error.getCode(), error.getMessage());
        });
        return request;
    }

    private QueryBuilder buildChannelCreateCommand(IDataHolder channelProps) {
        QueryBuilder request = IQueryRequest.builder();
        request.command(QueryCommands.CHANNEL.CHANNEL_CREATE);
        channelProps.getValues()
                .entrySet()
                .stream()
                .filter(e -> Objects.nonNull(e.getValue()))
                .forEach(entry -> request.addKey(entry.getKey(), entry.getValue()));
        request.addKey(PropertyKeys.Channel.FLAG_PERMANENT, "1");
        return request;
    }

    public void watchChannel(Integer channelId) {
        if (!watchedChannels.contains(channelId)) {
            watchedChannels.add(channelId);
        }
    }

    public void unwatchChannel(Integer channelId) {
        watchedChannels.remove(channelId);
    }
}
