package de.fearnixx.jeak.channelmgmt.overflow;

import de.fearnixx.jeak.channelmgmt.overflow.strategy.CountingStrategy;
import de.fearnixx.jeak.channelmgmt.overflow.strategy.OverflowStrategy;
import de.fearnixx.jeak.channelmgmt.overflow.strategy.RandomNameStrategy;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import de.mlessmann.confort.api.except.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

public class OverflowManager {

    private static final Logger logger = LoggerFactory.getLogger(OverflowManager.class);

    @Inject
    @Config(id = "overflow")
    public IConfig configRep;
    private IConfigNode config;

    @Inject
    public IEventService eventService;

    @Inject
    public IInjectionService injectionService;

    @Inject
    @LocaleUnit(value = "channelmgmt", defaultResource = "channelmgmt/lang.json")
    public ILocalizationUnit localeUnit;

    private boolean saveAfterInit = false;

    private final Map<Integer, List<String>> setsEnabledOn = Collections.synchronizedMap(new HashMap<>());
    private final Map<String, OverflowStrategy<?>> overflowSets = Collections.synchronizedMap(new HashMap<>());
    private OverflowWatcher watcher = new OverflowWatcher(this);

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
        }

        injectionService.injectInto(watcher);
        eventService.registerListener(watcher);
    }

    public boolean loadConfig() {
        try {
            configRep.load();
        } catch (FileNotFoundException e) {
            configRep.createRoot();
            saveAfterInit();

        } catch (IOException | ParseException e) {
            logger.error("Failed to read configuration: {}", e.getMessage(), e);

            // Fail-Fast: Rather throw many NPEs rather than overwriting the configuration
            config = null;
            return false;
        }

        config = configRep.getRoot();
        readConfig();

        if (saveAfterInit) {
            saveConfig();
        }

        return true;
    }

    private void readConfig() {
        IConfigNode setsNode = config.getNode("sets");
        setsNode
                .optMap()
                .orElseGet(Collections::emptyMap)
                .forEach((setName, setNode) -> {
                    String strategyType = setNode.getNode("strategyType").optString(null);
                    if (strategyType == null) {
                        logger.warn("Set has no strategyType: {}", setName);
                        return;
                    }

                    loadStrategy(setName, strategyType, setNode);
                });

        IConfigNode channelsNode = config.getNode("channels");
        channelsNode
                .optMap()
                .orElseGet(Collections::emptyMap)
                .forEach((channelIdStr, channelNode) -> {
                    Integer channelId;
                    try {
                        channelId = Integer.parseInt(channelIdStr);
                    } catch (NumberFormatException e) {
                        logger.warn("Cannot load overflow-setting for channel {}", channelIdStr, e);
                        return;
                    }

                    channelNode.getNode("sets")
                            .optList()
                            .orElseGet(Collections::emptyList)
                            .forEach(setNameNode -> {
                                String setName = setNameNode.optString(null);

                                if (setName == null) {
                                    logger.warn("Non-string encountered when reading set names for {}", channelIdStr);
                                } else if (!hasSet(setName)) {
                                    logger.warn("Invalid set name for channel {}: {}", channelIdStr, setName);

                                } else {
                                    enableSetOn(setName, channelId, false);
                                }
                            });

                    watcher.watchChannel(channelId);
                });

        if (!setsNode.isMap()) {
            setsNode.setMap();
            saveAfterInit();
        }

        if (!channelsNode.isMap()) {
            channelsNode.setMap();
            saveAfterInit();
        }

        IConfigNode adminsNode = config.getNode("admins");
        if (!adminsNode.isList()) {
            adminsNode.setList();
            saveAfterInit();
        }
    }

    private void loadStrategy(String name, String strategyType, IConfigNode node) {
        OverflowStrategy<?> strategy = null;
        if ("counting".equals(strategyType)) {
            strategy = new CountingStrategy();
        } else if ("randomName".equals(strategyType)) {
            strategy = new RandomNameStrategy(this);
        }

        if (strategy == null) {
            logger.error("Unable to load set {}: No strategy matching \"{}\"", name, strategyType);
        } else if (!strategy.fromNode(node)) {
            logger.error("Unable to load set {}: Strategy not loadable from: {}", name, strategy.getClass().getName());
        } else {
            overflowSets.put(name, strategy);
            logger.info("Loaded overflow set: {}", name);
        }
    }

    public boolean saveConfig() {
        try {
            configRep.save();
            return true;
        } catch (IOException e) {
            logger.error("Failed to save configuration: {}", e.getMessage(), e);
            return false;
        }
    }

    public void saveAfterInit() {
        saveAfterInit = true;
    }

    public boolean hasSet(String setName) {
        return overflowSets.containsKey(setName);
    }

    public void enableSetOn(String setName, Integer channelId) {
        enableSetOn(setName, channelId, true);
    }

    private void enableSetOn(String setName, Integer channelId, boolean save) {
        List<String> sets = setsEnabledOn.computeIfAbsent(channelId, cid -> new LinkedList<>());
        if (!sets.contains(setName)) {
            sets.add(setName);
        }

        if (save) {
            IConfigNode setEntry = config.createNewInstance();
            setEntry.setString(setName);
            config.getNode("channels", channelId.toString(), "sets")
                    .append(setEntry);
            saveConfig();
        }
    }

    public void runForChannel(Integer channelId, Consumer<OverflowStrategy<?>> action) {
        if (setsEnabledOn.containsKey(channelId)) {
            setsEnabledOn.get(channelId)
                    .stream()
                    .map(overflowSets::get)
                    .forEach(action);
        }
    }

    public boolean isAdmin(String clientUniqueID) {
        return config.getNode("admins")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(IConfigNode::isPrimitive)
                .map(IValueHolder::asString)
                .anyMatch(clientUniqueID::equals);
    }

    @Listener
    public void onChannelDeleted(IQueryEvent.INotification.IChannelDeleted event) {
        final Integer channelId = event.getTarget().getID();
        if (setsEnabledOn.containsKey(channelId)) {

            setsEnabledOn.remove(channelId);
            config.getNode("channels").remove(channelId.toString());
            saveConfig();
        }
    }
}
