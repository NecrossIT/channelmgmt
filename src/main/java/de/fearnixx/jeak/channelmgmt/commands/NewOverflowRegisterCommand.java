package de.fearnixx.jeak.channelmgmt.commands;

import de.fearnixx.jeak.channelmgmt.overflow.OverflowManager;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;

import java.util.Map;

public class NewOverflowRegisterCommand {

    private OverflowManager overflowManager;

    public static ICommandSpec getCommandSpec() {
        return Commands.commandSpec("overflow-register", "channelmgmt:overflow-register")
                .permission("channelmgmt.command.overflow_register")
                .parameters(
                        Commands.paramSpec("setName", String.class)
                )
                .executor(new NewOverflowRegisterCommand()::execute)
                .build();
    }

    public void execute(ICommandExecutionContext context) throws CommandException {
        IClient sender = context.getSender();

        String setName = context.getRequiredOne("setName", String.class);

        if (!overflowManager.hasSet(setName)) {
            throw new CommandParameterException(overflowManager.localeUnit.getContext(sender).getMessage("general.no_set_found"));
        }

        Integer channelId = sender.getChannelID();
        overflowManager.enableSetOn(setName, channelId);

        final var params = Map.of(
                "channelId", String.valueOf(channelId),
                "setName", setName
        );

        String localeMessage = overflowManager.localeUnit.getContext(sender).getMessage("overflow.channel_added_to_set", params);
        IQueryRequest sendMessage = sender.sendMessage(localeMessage);
        context.getConnection().sendRequest(sendMessage);
    }

    public void setManager(OverflowManager manager) {
        this.overflowManager = manager;
    }
}
