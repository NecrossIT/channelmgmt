package de.fearnixx.jeak.channelmgmt.commands;

import de.fearnixx.jeak.channelmgmt.ChannelManagement;
import de.fearnixx.jeak.channelmgmt.enforcement.EnforcementService;
import de.fearnixx.jeak.channelmgmt.enforcement.IEnforcementService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class NewEnforceRegisterCommand {

    @Inject
    public IEnforcementService enforcementService;

    private final Logger logger = LoggerFactory.getLogger(NewEnforceRegisterCommand.class);

    public static ICommandSpec getCommandSpec() {
        return Commands.commandSpec("enforce-register", "channelmgmt:enforce-register")
                .parameters(
                        Commands.paramSpec("channelName", String.class)
                )
                .permission("channelmgmt.command.enforce_register")
                .executor(new NewEnforceRegisterCommand()::execute)
                .build();
    }

    private void execute(ICommandExecutionContext context) throws CommandException {
        final IClient sender = context.getSender();

        final Integer channelId = sender.getChannelID();
        String name = context.getRequiredOne("channelName", String.class);

        if (!enforcementService.hasSet(name)) {
            throw new CommandParameterException(enforcementService.getLocaleUnit().getContext(sender).getMessage("general.no_set_found"));
        }

        enforcementService.enableSetOn(channelId, name);

        final var params = Map.of(
                "channelId", String.valueOf(channelId),
                "setName", name
        );

        String localeMessage = enforcementService.getLocaleUnit().getContext(sender).getMessage("enforce.channel_added_to_set", params);
        IQueryRequest sendMessage = sender.sendMessage(localeMessage);
        context.getConnection().sendRequest(sendMessage);
    }

    public void setService(EnforcementService service) {
        this.enforcementService = service;
    }
}
