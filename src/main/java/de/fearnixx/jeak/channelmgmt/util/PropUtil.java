package de.fearnixx.jeak.channelmgmt.util;

import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;

import java.util.Collections;
import java.util.List;

public class PropUtil {

    /**
     * Since TeamSpeak does not always provide consistency in naming, some fixes need to be applied.
     */
    public static void fixPropsForCreation(IDataHolder dataHolder) {

        dataHolder.getProperty(PropertyKeys.Channel.PARENT).ifPresent(parent -> {
            // When creating a channel, parents are set using "cpid" and not "pid".
            dataHolder.setProperty("cpid", parent);
            dataHolder.setProperty(PropertyKeys.Channel.PARENT, null);

            // channel_icon_id cannot be set in TS3 3.9.0+
            dataHolder.setProperty(PropertyKeys.Channel.ICON_ID, null);
        });
    }
}
